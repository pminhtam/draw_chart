requirements: matplotlib, pylab, re
"python chart.py": input in folder data/COMBINE, output in folder chart_output: get simple chart of all data
"python feats.py": get del_feats of all data (data in feats.py)
"python time_chart.py": get time chart of douban and allmovie dataset (input data in time_chart.py)
"python ablation_test.py": get ablation chart of allmovie and douban dataset (input data in ablation_test.py)