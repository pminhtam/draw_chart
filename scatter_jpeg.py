import matplotlib.pyplot as plot
import numpy as np
data_path = "data/jpeg_26082023"


final_data = []
count = 0
new_data = None
with open(data_path, 'r', encoding='utf-8') as file:
    for line in file:
        count += 1
        # print(count)
        # print(count)
        if (count - 1) % 8 == 0:
            if new_data is not None:
                final_data.append(new_data)
            new_data = []
        data_line = line.split()
        data_line = [float(ele) for ele in data_line]
        new_data.append(data_line)
#         if count == 49:
        if count == 64:

            final_data.append(new_data)
        # print(new_data)

final_data_np = np.array(final_data)
print(final_data)
box_sum = [[],[],[],[],[],[],[],[]]
for d in final_data:
    for i in range(len(d)):
        box_sum[i].extend(d[i])
gen_teches = ['Deepfake', '3DMM', 'FaceSwap-2D', 'FaceSwap-3D', 'MonkeyNet', 'ReenactGAN', 'StarGAN','X2Face']
# detech_teches =
detech_techs = ["Meso4", "XceptionNet", "GAN-fp", "FDBD", "HPBD", "VA","M2TR","MAT"]
# noises = ["0.5", "0.75", "1", "1.5", "2"]
noises = ["50", "60", "70", "80", "90", "100"]

indices = []
for i in range(1,9):
    indices = indices + [i] * 6
print(len(indices))
print(final_data_np[0].shape)
print(final_data_np[0].flatten().shape)
plot.boxplot(final_data[0], labels = detech_techs)
plot.xticks(rotation=25)
plot.scatter(indices, final_data_np[0].flatten(), c='r', zorder=10)

# plot.show()
colors = []
import matplotlib.cm as cm

# cs = cm.rainbow(np.linspace(0, 1, 6))
cs = ['#e1f5ff','#b4ebff','#96cdff','#649bff','#192df5','#0014dc']
for i in range(0,8):
    for j in range(0,6):
        colors.extend([cs[j]])
fig = plot.figure(figsize=(11,5))
# plot.boxplot(final_data[0], labels = detech_techs)
plot.boxplot(box_sum, labels = detech_techs)

plot.xticks(rotation=35)
plot.scatter(indices, final_data_np[0].flatten(), c=colors, zorder=2)
# plot.boxplot(final_data[1], labels = detech_techs)
plot.scatter(indices, final_data_np[1].flatten(), c=colors, zorder=2)
# plot.boxplot(final_data[2], labels = detech_techs)
plot.scatter(indices, final_data_np[2].flatten(), c=colors, zorder=2)
# plot.boxplot(final_data[3], labels = detech_techs)
plot.scatter(indices, final_data_np[3].flatten(), c=colors, zorder=2)
plot.scatter(indices, final_data_np[4].flatten(), c=colors, zorder=2)
plot.scatter(indices, final_data_np[5].flatten(), c=colors, zorder=2)
plot.scatter(indices, final_data_np[6].flatten(), c=colors, zorder=2)

plot.scatter(indices, final_data_np[7].flatten(), c=colors, zorder=2)
# plot.scatter(indices, final_data_np[8].flatten(), c=colors, zorder=2)

# plot.scatter(indices, final_data_np[7].flatten(), c=colors, zorder=2)
plot.yticks(fontsize=14)

# fig_legend.show()
plot.xticks(fontsize=13,fontweight='bold')
# fig_legend = plot.figure(figsize=(2,2))
# fig_legend.show()
# plot.legend(['50', '60', '70','80','90','100'],cs)
plot.savefig('jpeg_box2.png',dpi=150, bbox_inches='tight')
plot.savefig('jpeg_box2.pdf',dpi=150, bbox_inches='tight')